<?php
namespace App;

use App\Security\ForbiddenException;
use Exception;

class Auth {

    public static function check () {
        if (isset($_GET['admin'])) {
            throw new Exception("acces interdit ");
        }
       
        if (session_status() === PHP_SESSION_NONE) {
            session_start();
        }
        if(!isset($_SESSION['auth'])) {
            throw new ForbiddenException();
        }
    }
    
}
