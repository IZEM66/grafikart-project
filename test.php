<?php
function scanChar($s)
{
    $c = '';
    for ($c = 'A'; $c <= 'Z'; $c++) {

        if (asciiArt::printChar($c) == $s) {

            return $c;
        }
    }

    return null;
}
